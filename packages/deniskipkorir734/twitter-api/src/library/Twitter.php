<?php

namespace Deniskipkorir734\Twitter;
use Thujohn\Twitter\Twitter as TwitterAPI;
class Twitter
{
    protected $sentiments;
    protected $twitter;

    /**
     * @param $this
     * @return void
     */
    public function _construct($sents){
        $this->sentiments=$sents;
        $this->twitter=new TwitterAPI(config('twitter'));
    }

    private function writeTweets(){
        $this->twitter->postTweet(['status' => $this->sentiments]);
    }
}
